add_formatter_test(pep8 pass format.py pass)
add_formatter_test(pep8 format format.py format)
add_formatter_test(pep8 with-config format.py pass)
