set(_test_formatter_script "${CMAKE_CURRENT_SOURCE_DIR}/test-formatter.sh")

function (add_formatter_test formatter name path expect)
  add_test(
    NAME    "${formatter}-${name}"
    COMMAND "${_test_formatter_script}"
            "${format_${formatter}_script}"
            "${path}"
            "${expect}"
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${name}")
endfunction ()

get_property(formatters GLOBAL PROPERTY formatters)
foreach (formatter IN LISTS formatters)
  get_property(have_tools GLOBAL
    PROPERTY "formatter_${formatter}_tools")
  get_property("format_${formatter}_script" GLOBAL
    PROPERTY "formatter_${formatter}_script")

  add_formatter_test("${formatter}" no-file noexist.txt fail)

  if (NOT have_tools)
    add_formatter_test("${formatter}" no-tools "format.txt" "fail")
    continue ()
  endif ()

  add_subdirectory("${formatter}")
endforeach ()
