#!/bin/sh

set -e

readonly formatter="$1"
shift

readonly path="$1"
shift

readonly expect="$1"
shift

# Allow errors here.
set +e
"$formatter" "$path"
readonly exit_code="$?"
set -e

case "$expect" in
    pass)
        expect_error=false
        expect_diff=false
        ;;
    format)
        expect_error=false
        expect_diff=true
        ;;
    fail)
        expect_error=true
        expect_diff=false
        ;;
esac
readonly expect_error
readonly expect_diff

test_result=true

pass () {
    echo "success:" "$@"
}

fail () {
    test_result=false
    echo >&2 "error:" "$@"
}

if $expect_error; then
    if [ "$exit_code" = "0" ]; then
        fail "expected the tool to exit with failure"
    else
        pass "the tool exited with failure"
    fi
else
    if [ "$exit_code" = "0" ]; then
        pass "the tool exited successfully"
    else
        fail "expected the tool to exit successfully"
    fi
fi

if [ -f "$path" ]; then
    if $expect_diff; then
        if diff -q "$path" "$path.clean"; then
            fail "the file was not formatted by the tool"
        else
            if diff -q "$path" "$path.expect"; then
                pass "the file was formatted properly"
                # Move the clean file back so that tests are consistent.
                cp "$path.clean" "$path"
            else
                fail "the file was not formatted properly"
            fi
        fi
    else
        if diff -q "$path" "$path.clean"; then
            pass "the file is clean"
        else
            fail "expected the file to be properly formatted"
        fi
    fi
fi

exec "$test_result"
